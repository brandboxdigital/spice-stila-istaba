export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: "server",
  // ssr: true,

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { hid: "og:title", property:"og:title", content:"" },
      { hid: "og:description", property:"og:description", content: "" },
			{ hid: "og:image", property:"og:image", content: "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: 'stylesheet', type: 'text/css', href: '/css/fonts.css'},
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/zwr6wcv.css' }
    ],
  },


  publicRuntimeConfig: {
    APP_DOMAIN: process.env.APP_DOMAIN || "spice-clone.brandbox.digital",
    gtm: {
      id: process.env.GOOGLE_TAG_MANAGER_ID
    }
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    "@/assets/styles/main.scss",
    "vue-slick-carousel/dist/vue-slick-carousel.css",
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "./plugins/vue-slick-carousel.js" },
    { src: "./plugins/axios.js" },
    { src: "./plugins/globalFilters.js" },
    { src: "~/plugins/vue-lazy-image.js" },
    { src: "~/plugins/vee-validate.js"},
    { src: "~/plugins/imagekit.js"},
    // { src: "~/plugins/sticky-directive.js", mode: 'client'}
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: {
    dirs: [
      "~/components",
      "~/components/base",
      "~/components/partials",
      "~/components/assets",
      // "~/components/widgets"
    ]
  },

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    "@nuxtjs/style-resources",
    'nuxt-gsap-module'
  ],

  styleResources: {
    scss: [
      "@/assets/_variables.scss",
      "@/assets/_fonts.scss",
      '@/assets/styles/_bulma_utilities.scss'
    ]
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/gtm',
    "nuxt-i18n",
    "@nuxtjs/axios",
    "nuxt-user-agent",
    "nuxt-mq",
    "@nuxtjs/sitemap",
    "nuxt-ssr-cache",
    ['nuxt-buefy'],
    // 'nuxt-lazy-load',

    '@nuxtjs/sentry',
  ],

  sentry: {
    dsn: process.env.SENTRY_DSN || 'https://8b8b1e6c33517ebd2f0c408db3ba43a7@o213790.ingest.sentry.io/4505640990015488', // Enter your project's DSN.

    tracing: false,
    tracesSampleRate: 0.2,
    browserTracing: {},
    vueOptions: {
      trackComponents: true,
    },
  },

  gsap: {
    /* Module Options */
    extraPlugins: {
      scrollTrigger: true
    },
    extraEases: {
      slowMo: true
    }
  },

  mq: {
    defaultBreakpoint: "md",
    breakpoints: {
      mobile: 950,
      md: 2000,
      lg: Infinity
    }
  },

  i18n: {
    defaultLocale: "lv",
    strategy: "prefix",
    locales: [
      { code: "lv", file: "lv.json", name: "Latviski", iso: "lv-LV", isCatchallLocale: true },
      { code: "en", file: "en.json", name: "English", iso: "en-US" },
      { code: "ru", file: "ru.json", name: "По-русски", iso: "ru-RU" },
    ],
    lazy: true,
    langDir: "locales/",
    seo: true,

    detectBrowserLanguage: false,
  },

  gtm: {
    // id: "GTM-MSSFD4B", // Used as fallback if no runtime config is provided
    id: 'GTM-MHK8P46', // Used as fallback if no runtime config is provided
    // enable: true,
    // debug: true,
  },

  // bootstrapVue: {
  //   bootstrapCSS: false, // Or `css: false`
  //   bootstrapVueCSS: false // Or `bvCSS: false`
  // },

  axios: {
    // baseURL: "http://spice-cms.test/api/v1",
    // baseURL: process.env.BACKEND_URL || "https://spice-clone.brandbox.digital/api/v1",
    // baseURL: process.env.BACKEND_URL || "https://api.spicestyle.lv/api/v1",
    //baseURL: process.env.BACKEND_URL || "https://cms.spice.lv/api/v1",
    credentials: true,
    withCredentials: true,

    proxy: true,
    proxyHeaders: false,
  },

  proxy: {
    '/api/': process.env.BACKEND_URL || 'https://cms.spice.lv',
    '/storage/': process.env.BACKEND_URL || 'https://cms.spice.lv',

    '/v1/': (process.env.BACKEND_URL || 'https://cms.spice.lv') + '/api/',
  },

  version: 1.5,

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ]
  },
  server: {
    port: process.env.PORT || 3000, // default: 3000,
    host: process.env.HOST || "0.0.0.0" // default: localhost
  },

};
