export const state = () => ({
    loading: true,
    fixPage: true,
    images: [],
    cursorEffects: false
})

export const mutations = {
  applyCursorEffects: (state,payload) => {
    state.cursorEffects = payload
  },
  loading: (state) => {
      state.loading = !state.loading
  },
  fixPage: (state) => {
    state.fixPage = !state.fixPage
  },
  initImages: (state, images) => {
      state.images = images.data
  },

}
