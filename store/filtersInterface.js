
export const state = () => ({
  filters: {
    floor_filter_interface: {
      title: 'filters_interface.floor_available',
      slug: 'floor_filter_interface',
      type: 'radio',
      choices: [
        {
          translate: true,
          title: 'floor_5',
          values: 5
          //values: 5,
        },
        {
          translate: true,
          title: 'floor_4',
          values: 4
          //values: 4,
        },
        {
          translate: true,
          title: 'floor_3',
          values: 3
          //values: 3,
        },
        {
          translate: true,
          title: 'floor_2',
          values: 2
          //values: 2,
        },
        {
          translate: true,
          title: 'floor_1',
          values: 1
          //values: 1,
        }
      ],
      value: 1
    }
  }
})

export const mutations = {
  setFilters: (state,payload) => {
    state.filters[payload.filter.slug]['value'] = payload.newVal
  }
}



