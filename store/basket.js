import _get from "lodash/get";

export const state = () => ({
  delivery: null,
  payment:  null,

  isLoading: false,
  syncedAt: null,

  cart: null,

  discountCode: null,
})

export const mutations = {

  /**
   *
   * @param {object} state vuex state
   * @param {object} cart  card object, that comdes form API
   */
  setCart: (state, cart) => {
    if (cart && cart.id) {
      state.cart = cart;
      state.syncedAt = new Date;
    }
  },

  /**
   *
   * @param {object} state vuex state
   * @param {object} delivery
   */
  setDelivery: (state, delivery) => {
    state.delivery = delivery;
  },

  /**
   *
   * @param {object} state vuex state
   * @param {object} payment
   */
  setPayment: (state, payment) => {
    state.payment = payment;
  },

  // EMPTY_LIST: (state) => {
  //   state.list = []
  // },

  START_LOADING: (state) => {
    state.isLoading = true;
  },
  STOP_LOADING: (state) => {
    state.isLoading = false;
  },
}

export const getters = {
  isLoading: (state) => {
    return state.isLoading;
  },

  list(state, getters) {
    return _get(state, 'cart.items', [])
  },

  giftCardList(state, getters) {
    return _get(state, 'cart.gift_cards', [])
  },

  discountList(state, getters) {
    return _get(state, 'cart.totals.appliedDiscounts', []);
  },
  discount(state, getters) {
    return _get(state, 'cart.totals.totalDiscounts', 0);
  },
  count(state, getters) {
    var total = 0;
    if (getters.list.length > 0) {
      for (var i = getters.list.length - 1; i >= 0; i--) {
        total = total + Number(getters.list[i].count)
      }
    }
    if (getters.giftCardList.length > 0) {
      for (var i = getters.giftCardList.length - 1; i >= 0; i--) {
        total = total + 1;
      }
    }
    return total;
    // return getters.list.length + getters.giftCardList.length
  },

  subtotal(state, getters) {
    const items = getters.list.reduce((acc, cur) => acc + (cur.variation.price.amount * cur.count), 0)
    const giftCards = getters.giftCardList.reduce((acc, cur) => acc + (cur.value), 0);
    return parseInt(items) + parseInt(giftCards);
  },

  total(state, getters) {
    return Math.max(getters.subtotal - getters.discount + getters.deliverySumm, 0);
  },

  // deliverySumm: state => (state.delivery) ? parseInt(state.delivery.price) : null,
  deliverySumm: state => (state.delivery) ? parseInt(state.delivery.price.amount) : null,
  paymentSumm: state => (state.payment) ? parseInt(state.payment.price) : null,

  isDiscountBoxDisabled: state => (state.cart) ? state.cart.is_discount_box_disabled : true,
}


export const actions = {
  ADD_PRODUCT (state, {variation}) {
    state.commit( 'START_LOADING' );

    this.$axios.$post('/v1/cart/item/add', {
      variation_id: variation.id
    }).then((result) => {
      state.commit( 'setCart', result.data);

    }).catch((err) => {

    }).finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  CHANGE_COUNT (state, { variation, count }) {
    state.commit( 'START_LOADING' );

    this.$axios.$post('/v1/cart/item/update', {
      variation_id: variation.id,
      count: count,
    }).then((result) => {

      state.commit( 'setCart', result.data);

    }).catch((err) => {

    }).finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  REMOVE_PRODUCT (state, variation) {
    state.commit( 'START_LOADING' );

    this.$axios.$post('/v1/cart/item/remove', {
      variation_id: variation.id,
    }).then((result) => {

      state.commit( 'setCart', result.data);

    }).catch((err) => {

    }).finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
    // state.list.splice(index, 1);
    // state.list = state.list.filter(product => product.id !== id)
  },


  SET_DELIVERY(state, delivery) {
    state.commit( 'START_LOADING' );

    this.$axios.$post('/v1/cart/set-delivery', {
      delivery: delivery,
    })
    .then((result) => {

      state.commit('setDelivery',  delivery);

    })
    .catch((err) => {})
    .finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  SET_PAYMENT(state, payment) {
    state.commit( 'START_LOADING' );

    this.$axios.$post('/v1/cart/set-payment', {
      payment: payment,
    })
    .then((result) => {
      state.commit('setPayment', payment);
    })
    .catch((err) => {})
    .finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  ADD_USER_DISCOUNT(state, user) {
    this.$axios.$post('/v1/cart/add-discount/user', {
      user: user,
    })
    .then((result) => {
      state.commit( 'setCart', result.data);

      if (result && result.error == false) {
        state.discountCode = null;
      }
    })
    .catch((err) => {})
    .finally((stuff) => {
    });
  },

  ADD_DISCOUNT_CODE(state, code) {
    state.commit( 'START_LOADING' );
    this.$axios.$post('/v1/cart/add-discount/code', {
      code: code,
    })
    .then((result) => {
      state.commit( 'setCart', result.data);

      if (result && result.error == false) {
        state.discountCode = code;
      }
    })
    .catch((err) => {})
    .finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  /**
   * Wrapped in another Promise so that it can be chained
   * where it was dsipatched
   */
  ADD_GIFT_CARD(state, card) {
    state.commit( 'START_LOADING' );
    return new Promise((resolve, reject) => {

      this.$axios.$post('/v1/cart/gift-card/add', {
        card: card,
      })
      .then((result) => {
        state.commit( 'setCart', result.data);

        if (result.error) {
          reject(result);
        }

        resolve(result);
      })
      .catch((err) => { reject(err); })
      .finally((stuff) => {
        state.commit( 'STOP_LOADING' );
      });

    })
  },

  REMOVE_GIFT_CARD(state, card) {
    state.commit( 'START_LOADING' );
    this.$axios.$post('/v1/cart/gift-card/remove', {
      card: card,
    })
    .then((result) => {
      state.commit( 'setCart', result.data);
    })
    .catch((err) => { reject(err); })
    .finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  REMOVE_DISCOUNT(state, discount) {
    state.commit( 'START_LOADING' );
    this.$axios.$post('/v1/cart/remove-discount', {
      id: discount.id,
    })
    .then((result) => {
      state.commit( 'setCart', result.data);
    })
    .catch((err) => {})
    .finally((stuff) => {
      state.commit( 'STOP_LOADING' );
    });
  },

  EMPTY_LIST(state) {
    state.commit( 'START_LOADING' );

    this.$axios.$post('/v1/cart/item/remove_all').then((result) => {

      state.commit( 'setCart', result.data);

      }).catch((err) => {

      }).finally((stuff) => {
        state.commit( 'STOP_LOADING' );
      });
  },

  async loadCart (state, context) {
    const cart = await this.$axios.$get('/v1/cart/get')
    state.commit( 'setCart', cart.data);
  },

  // async restoreCart(state, hash) {
  //   const cart = await this.$axios.$post('/v1/cart/restore', {
  //     hash: hash,
  //   });

  //   if (!cart.error) {
  //     state.commit( 'setCart', cart.data);
  //   }
  // },
}
