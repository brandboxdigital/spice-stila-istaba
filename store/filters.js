
export const state = () => ({
  filters: {
    pay_filter: {
      title: 'filter_list.pay_type',
      slug: 'pay_filter',
      type: 'checkbox',
      choices: [
        {
          translate: true,
          title: 'filter_list.rent',
          values: {
            title: 'filter_list.rent',
            values: 1
          }
          // values: 1
        },
        {
          translate: true,
          title: 'filter_list.buy',
          values: {
            title: 'filter_list.buy',
            values: 0
          }
          //values: 0
        },
      ],
      values: []
    },
    rooms_filter: {
      title: 'filter_list.bedrooms',
      slug: 'rooms_filter',
      type: 'checkbox-square',
      choices: [
        {
          title: '1',
          values: {
            title: '1',
            values: 1
          }
          //values: 1
        },
        {
          title: '2',
          values: {
            title: '2',
            values: 2
          }
          //values: 2
        },
        {
          title: '3',
          values: {
            title: '3',
            values: 3
          }
          //values: 3
        }
      ],
      values: []
    },
    address_filter: {
      title: 'filter_list.address',
      slug: 'address_filter',
      type: 'checkbox',
      choices: [
        {
          translate: true,
          title: 'maldugunu_6',
          //values: [1,4]
          values: {
            title: 'maldugunu_6',
            values: [1,4]
          }
        },
        {
          translate: true,
          title: 'maldugunu_8',
          //values: [7]
          values: {
            title: 'maldugunu_8',
            values: [7]
          }
        },
        {
          translate: true,
          title: 'maldugunu_10',
          //values: [2,5]
          values: {
            title: 'maldugunu_10',
            values: [2,5]
          }
        },
        {
          translate: true,
          title: 'maldugunu_12',
          //values: [3,6],
          values: {
            title: 'maldugunu_12',
            values: [3,6]
          }
        },
        {
          translate: true,
          title: 'daibes_31',
          //values: [8],
          values: {
            title: 'daibes_31',
            values: [8]
          }
        },
        {
          translate: true,
          title: 'daibes_33',
          //values: [9],
          values: {
            title: 'daibes_33',
            values: [9]
          }
        }
      ],
      values: []
    },
    floor_filter: {
      title: 'filter_list.floor_full',
      slug: 'floor_filter',
      type: 'checkbox',
      choices: [
        {
          translate: true,
          title: 'floor_1',
          values: {
            title: 'floor_1',
            values: 1
          }
          //values: 1,
        },
        {
          translate: true,
          title: 'floor_2',
          values: {
            title: 'floor_2',
            values: 2
          }
          //values: 2,
        },
        {
          translate: true,
          title: 'floor_3',
          values: {
            title: 'floor_3',
            values: 3
          }
          //values: 3,
        },
        {
          translate: true,
          title: 'floor_4',
          values: {
            title: 'floor_4',
            values: 4
          }
          //values: 4,
        },
        {
          translate: true,
          title: 'floor_5',
          values: {
            title: 'floor_5',
            values: 5
          }
          //values: 5,
        }
      ],
      values: []
    },
    garden_filter: {
      title: 'filter_list.extensions',
      slug: 'garden_filter',
      type: 'switch',
      text: 'filter_list.has_garden',
      values: false,
      hasFakeSwitch: true,
      fakeVal: {
        values: true,
        text: 'filter_list.has_balcony'
      }
    },
    area_filter: {
      title: 'filter_list.area_full',
      slug: 'area_filter',
      type: 'range',
      minVal: 50,
      //this.getMin('total_area'),
      maxVal: 160,
      //this.getMax('total_area'),
      values: [50,160]
    },
    block_filter: {
      title: 'filter_list.block_type',
      slug: 'block_filter',
      type: 'checkbox-square',
      choices: [
        {
          title: 'TH',
          values: {
            title: 'TH',
            values: [4,5,6]
          }
          // values: [4,5,6]
        },
        {
          title: 'CH',
          values: {
            title: 'CH',
            values: [1,2,3]
          }
          //values: [1,2,3]
        },
        {
          title: 'VL',
          values: {
            title: 'VL',
            values: [8,9]
          }
          // values: [8,9]
        },
        {
          title: 'YH',
          values: {
            title: 'YH',
            values: [7]
          }
          // values: [7]
        },
      ],
      values: []
    },
  }
})

export const mutations = {

  setFilters: (state,payload) => {
    state.filters[payload.filter.slug]['values'] = payload.newVal
  }
  // loading: (state) => {
  //     state.loading = !state.loading
  // },


}



