# Build
# FROM node:14-alpine as builder
# FROM --platform=linux/arm64 arm64v8/node:14-alpine as builder
# WORKDIR /app
# ADD . /app
# # RUN rm -fr node_modules
# # RUN apk --no-cache add openssh g++ make python3 git
# # COPY package.json /app/
# RUN yarn install
# RUN yarn build

# Run
# FROM node:14-alpine
FROM --platform=linux/arm64 arm64v8/node:14-alpine
WORKDIR /app

# COPY --from=builder /app /app
COPY . /app

# RUN yarn build

EXPOSE 3000

# ENTRYPOINT ["node", ".nuxt/server/index.mjs"]
CMD [ "yarn", "start" ]
