import Vue from "vue";

function fStatically(url, width, height)  {

  // Check if we need to run Nuxt in development mode
  const isDev = process.env.NODE_ENV !== 'production'

  if (isDev) {
    return url;
  }

  /**
   * Already is stacally link..
   */
  if (url.indexOf('cdn.statically.io') !== -1) {
    return url;
  }

  let link   = url;
  let domain = "spice-clone.brandbox.digital";

  try {
     domain = $nuxt.$config.APP_DOMAIN || "spice-clone.brandbox.digital";
  } catch (error) {}

  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
  } else {
    const noHttp = url.replace('http://','').replace('https://','');

    domain = noHttp.split(/[/?#]/)[0];
    link   = noHttp.replace(domain+"/", '');
  }

  if (width && height) {
    return `https://cdn.statically.io/img/demo/${domain}/q=90,f=auto,w=${width},h=${height}/${link}`;
  } else if (width) {
    return `https://cdn.statically.io/img/demo/${domain}/q=90,f=auto,w=${width}/${link}`;
  } else {
    return `https://cdn.statically.io/img/demo/${domain}/q=90,f=auto/${link}`;
  }

}


if (!Vue.__my_mixin__) {
  Vue.__my_mixin__ = true

  Vue.filter("statically", function(url, width, height)  {
    return fStatically(url, width, height);
  });

  Vue.mixin({

    methods: {
      g_splitPrice(price) {
        let split = price.split('.')
        return split
      },
      g_transformPrice(p) {
        let price = p.toString()
        price = price.slice(0,price.length-2) +'.'+ price.slice(price.length-2,price.length)
        price = price.replace(' ', '')
        return price
      },
      g_getNumberWithZero(number) {
        if (number < 10) {
          return '0' + number.toString()
        }
        return number.toString()
      },
      g_putInQuotes(text) {
        return '“' + text + "”"
      }
    },
  })

};
