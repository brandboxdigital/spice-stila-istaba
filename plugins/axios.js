export default function ({ $axios, $auth, redirect, app }) {

    $axios.onRequest(config => {
        config.headers.common['Accept'] = 'application/json';
        config.headers.common['Content-Type'] = 'application/json';
        config.headers.common['Accept-Language'] = app.i18n.locale;
    });
  
    $axios.onError(error => {
        console.error("Axios",error);
    })
  
}
  
  