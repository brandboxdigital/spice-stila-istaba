export default (context, inject) => {

  function fImageKit(url, width, height)  {

    //console.log("fImageKit", url);

    if (!url) {
      return '';
    }

    if (typeof url === 'object') {

      if (Object.keys(url).indexOf('path') > -1) {
        url = url.path;
      } else {
        return '';
      }
    }

    // Check if we need to run Nuxt in development mode
    const isDev = process.env.NODE_ENV !== 'production'

    // if (isDev || process.env.DISABLE__STATICALLY) {
    //   return url;
    // }

    /**
     * Already is stacally link..
     */
    if (url.indexOf('ik.imagekit.io') !== -1) {
      return url;
    }

    let link   = url;
    let domain = 'localhost';

    try {
       domain = $nuxt.$config.TRUE_HOST;
    } catch (error) {}

    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
    } else {
      const noHttp = url.replace('http://','').replace('https://','');

      domain = noHttp.split(/[/?#]/)[0];
      link   = noHttp.replace(domain+"/", '');
    }

    try {
      domain = domain.replace('http://','').replace('https://','').replace(/\/$/, '');
    } catch (error) {}

    const imageKitEndpointName = 'spicestyle';

    // https://cms.balturotas.brandbox.digital/storage/app/uploads/public/61c/06c/5e7/61c06c5e760bf854907992.jpg
    // https://ik.imagekit.io/bboxdigi/balturotas/storage/app/uploads/public/61c/06c/5e7/61c06c5e760bf854907992.jpg?tr=2-300,h-300

    if (width && height) {
      return `https://ik.imagekit.io/bboxdigi/${imageKitEndpointName}/${link}?tr=c-at_max,w-${width},h-${height}`;
    } else if (width) {
      return `https://ik.imagekit.io/bboxdigi/${imageKitEndpointName}/${link}?tr=c-at_max,w-${width}`;
    } else {
      return `https://ik.imagekit.io/bboxdigi/${imageKitEndpointName}/${link}`;
    }

    // if (width && height) {
    //   return `https://cdn.statically.io/img/${domain}/q=90,f=auto,w=${width},h=${height}/${link}`;
    // } else if (width) {
    //   return `https://cdn.statically.io/img/${domain}/q=90,f=auto,w=${width}/${link}`;
    // } else {
    //   return `https://cdn.statically.io/img/${domain}/q=90,f=auto/${link}`;
    // }

  }

  inject('imagekit', fImageKit)
}
