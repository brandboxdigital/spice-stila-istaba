import Vue from 'vue'
import Sticky from 'vue-sticky-directive'
// import VueSticky from 'vue-sticky' // Es6 module

Vue.use(Sticky);
