import Vue from 'vue'
import { extend, localize } from "vee-validate";
import { required, alpha, digits, email } from "vee-validate/dist/rules";
import { ValidationProvider, ValidationObserver } from "vee-validate";

import lv from 'vee-validate/dist/locale/lv.json';

extend('email', {
  ...email,
  message: "email"
});

extend('required', required);
extend('emailReq', {
  ...required,
  message: "email"
});
// extend("required", {
//   ...required,
//   message: "ir obligāts"
// });

extend("name", {
  ...required,
  message: "name"
})

extend("alpha", {
  ...alpha,
  message: "Laukam jāsastāv no alfabēta simboliem"
});

extend('phone', {
  validate: value => {
    return value.length >= 8
  },
  message: "phone",
});

extend('phoneReq', {
  ...required,
  message: "phone",
});

extend('stylist', {
  ...required,
  message: "stylist",
});

extend('privacy', {
  validate: value => {
    return value == true
  },
  message: "privacy",
});

extend('terms', {
  validate: value => {
    return value == true
  },
  message: "terms",
});

extend('gift-card-value', {
  validate: value => {
    return value > 0;
  },
  message: "Vērtība nevar būt 0",
});


localize('lv', lv);

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

